ServiceConfiguration.configurations.upsert({
  service: "google"
}, {
  $set: {
    appId: Meteor.settings.google.appId,
    loginStyle: "popup",
    secret: Meteor.settings.google.secret
  }
});