import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Campaigns = new Mongo.Collection('campaigns');

Meteor.methods({
  'campaigns.find'() {
    return Campaigns.find({}).fetch();
  },
  'campaigns.trending'() {
    return Campaigns.find({trending: true}, {limit: 5}).fetch();
  }
});