import React from 'react';
import styled from 'styled-components';
import Tabs from '@atlaskit/tabs';
import Header from './components/Header';
import OverView from './Page/OverView';

const Section = styled.section`
  margin: 20px auto;
  width: 90%;

`;

class Page extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const tabs = [
      { label: 'Overview', content: <OverView /> },
      { label: 'Updates', content: <p>Updates</p> },
      { label: 'Contributors', content: <p>Contributors</p> },
      { label: 'Comments', content: <p>Comments</p> },
    ];
    return (
      <Section>
        <Header title="Google Glass" subtitle="Sundarbans, Kolkata" />
        <Tabs
          tabs={tabs}
          onSelect={(tab, index) => console.log('Selected Tab', index + 1)}
        />
      </Section>
    );
  }
}

export default Page;