import React from 'react';
import PropTypes from "prop-types";
import styled from 'styled-components';
import SubHeading from '../Subheading';
import ProgressBar from '../ProgressBar';

const CardStyle = styled.article`
  width: 100%;
  height: 18rem;
  margin: 2em 0;
  
  .card {
    display: flex;
    position: relative;
    margin: 0 1em;
    cursor: pointer;
  }

  .image-container {
    background-image: url('https://source.unsplash.com/user/erondu/300x300');
    background-repeat: no-repeat;
    background-size: cover;
    width: 40%;
    height: 100%;
  }

  .content {
    padding: 0.5em;
    width: 50%;
  }

  .description {
    margin: 0.4rem 0;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  
  .footer {
    width: 20rem;
    margin-top: 0.4rem;
    table-layout: fixed;

    td {
      overflow: hidden;
    }

    table.fixed td:nth-of-type(1) {
      width: 10rem;
    }
  }
`;

function LargeCard(props) {
  const {title, subtitle, description, amount, width, days, backers, currency} = props;
  return (
    <CardStyle>
      <div className="card">
        <div className="image-container" />
        <div className="content">
          <SubHeading>{title}</SubHeading>
          <p className='subheading'>{subtitle}</p>
          <p className="description">
            {description}
          </p>
          <div className="center-block">
            <SubHeading>{amount+' '+currency}</SubHeading>
            <ProgressBar width={width} />
          </div>
          <table className="footer">
            <tbody>
              <tr>
                <td>Days Remaining</td>
                <td>{days}</td>
              </tr>
              <tr>
                <td>Backers</td>
                <td>{backers}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </CardStyle>
  );
}

LargeCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  backers: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  days: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired
}


export default LargeCard;

