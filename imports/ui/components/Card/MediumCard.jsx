import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Subheading from '../Subheading';
import ProgressBar from '../ProgressBar';

const CardStyle = styled.article`
  width: 100%;

  .card {
    border: 1px solid #E0E0E0;
    margin: 0 1em;
    cursor: pointer;
  }

  .image-container {
    background-image: url(${props=>props.image});
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 8rem;
  }

  .subtitle {
    margin-top: 0;
  }

  .content {
    padding: 0.2rem 0.5rem;
  }

  .description {
    position:relative;
    line-height:1.4em;
    height:4.2em;
    overflow: hidden;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;

    &::after {
      content: '...';
      position: absolute;
      bottom:0;
      right: 2px;
    }
  }

  .footer {
    width: 100%;
    border-top: 1px solid #E0E0E0;
  }
`;

function MediumCard(props) {
  const {title, subtitle, description, amount, width, days, backers, image, currency} = props;
  return (
    <CardStyle image={image}>
      <div className="card">
        <div className="image-container" />
        <ProgressBar width={width} />
        <div className="content">
          <Subheading fontSize={1.2}>{title}</Subheading>
          <p className='subtitle'>{subtitle}</p>
          <p className='description'>{description}</p>
          <Subheading>{amount+' '+currency}</Subheading>
          <table className="footer">
              <tbody>
                <tr>
                  <td>Days Remaining</td>
                  <td>{days}</td>
                </tr>
                <tr>
                  <td>Backers</td>
                  <td>{backers}</td>
                </tr>
              </tbody>
            </table>
          </div>
      </div>
    </CardStyle>
  );
}

MediumCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  backers: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  days: PropTypes.number.isRequired
}

export default MediumCard;

