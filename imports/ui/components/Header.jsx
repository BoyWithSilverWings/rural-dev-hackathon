import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Heading from './Heading';

const Section = styled.header`
  text-align: center;
  padding-bottom: 1em;
  border-bottom: 2px solid #1DE9B6;

  h1 {
    margin-bottom: 5px;
  }
  
  p {
    margin: 5px 0;
  }
  
`;

function Header({className, title, subtitle}) {
  return (
    <Section className={className}>
        <Heading fontSize="2.3">{title}</Heading>
        {subtitle && <p>{subtitle}</p>}
    </Section>
  );
}

Header.defaultProps = {
  className: '',
  subtitle: null
}

Header.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string
}

export default Header;

