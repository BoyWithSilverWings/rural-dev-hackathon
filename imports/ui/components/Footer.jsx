import React from 'react';
import styled from 'styled-components';

const Section = styled.footer`
  width: 100%;
  min-height: 5rem;
  background-color: #000;
`;

function Footer() {
  return (
    <Section>
    </Section>
  );
}

export default Footer;

