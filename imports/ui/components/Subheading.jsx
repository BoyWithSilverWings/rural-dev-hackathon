import React from 'react';
import styled from 'styled-components';

const Subheading = styled.h1`
  font-size: ${props=>props.fontSize+'rem'};
  font-weight: 400;
  color: ${props=>props.color};
  font-family: 'Oswald', sans-serif;
  margin: ${props=>props.margin};
  text-transform: capitalize;
`;

Subheading.defaultProps = {
  color: '#000000',
  fontSize: 1.5,
  margin: 0
}

export default Subheading;

