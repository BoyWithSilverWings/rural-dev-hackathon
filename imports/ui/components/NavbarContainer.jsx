import React from 'react';
import {Link} from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import Button from './Button';
import Navbar from './Navbar';
import theme from '../constants/theme';
import AccountsUIWrapper from './AccountsUIWrapper';
import logo from './logo.svg';

function NavbarContainer(props) {
  return (
    <Navbar>
      <Link to="/" className='unstyled-link logo'>
        <img src={logo} alt="Logo"/>
      </Link>
      <Link to="/discover" className='unstyled-link'>
        <span>Discover</span>
      </Link>
      <Link to="/" className='unstyled-link'>
        <span>Dashboard</span>
      </Link>
      <Link to="/how-it-works" className='unstyled-link'>
        <span>How it Works</span>
      </Link>
      <div className="right-align">
          <AccountsUIWrapper />
      </div>
    </Navbar>
  );
}

NavbarContainer.defaultProps = {
  currentUser: null
}

export default withTracker(()=>{
  return {
    currentUser: Meteor.user()
  }
})(NavbarContainer);
