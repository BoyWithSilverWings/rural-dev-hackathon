import React from 'react';
import styled from 'styled-components';

export default styled.nav`
  background-color: white;
  border-bottom: 1px solid #E0E0E0;
  min-height: 3rem;
  padding: 0.2em 0.5em;
  display: flex;
  align-items: center;
  width: 100%;

  .logo {
    margin: 0 1.5em;
    img {
      height: 2.5rem;
    }
  }
  
  .unstyled-link {
    color: unset;
    margin: 0 1em;
    text-decoration: none;
    font-weight: bold;
  }

  .unstyled-link:hover,
  .unstyled-link:active,
  .unstyled-link:visited {
    color: unset;
    text-decoration: none;
  }

  .right-align {
    margin-left: auto;
    margin-right: 3em;
  }

`;