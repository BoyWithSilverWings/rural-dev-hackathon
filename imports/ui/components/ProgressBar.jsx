import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Outbar = styled.div`
  width: 100%;
  position: relative;
  background-color: #E0E0E0;
  height: 0.4rem;
  border-radius: 10px;
  overflow: hidden;
  margin-bottom: 1em;

  .done {
    width: ${props=>props.width};
    height: 100%;
    background-color: #00E676;
  }
`;

class ProgressBar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Outbar width={parseInt(parseInt(this.props.width)+2)+'%'}>
        <div className="done"></div>
      </Outbar>
    );
  }
}

ProgressBar.propTypes = {
  width: PropTypes.number.isRequired
}

export default ProgressBar;