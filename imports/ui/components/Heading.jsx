import React from 'react';
import styled from 'styled-components';

const Heading = styled.h1`
  font-size: ${props=>props.fontSize}rem;
  font-family: 'Oswald', sans-serif;
  letter-spacing: 4px;
  text-transform: uppercase;
  color: ${props=>props.color}
`;

Heading.defaultProps = {
  color: '#000000',
  fontSize: 4
}

export default Heading;

