import React from 'react';
import PropTypes from 'prop-types';
import differenceInDays from 'date-fns/difference_in_days'
import LargeCard from './Card/LargeCard';
import MediumCard from './Card/MediumCard';

function Card(props) {
  const {size, amount, total, ...rest} = props;
  const percentage = amount/total*100;
  const days = differenceInDays(props.date, new Date());
  
  if(size==='large') {
    return <LargeCard days={days} width={percentage} amount={amount} {...rest} />
  } else {
    return <MediumCard days={days} width={percentage} amount={amount} {...rest} />
  }
}

Card.defaultProps = {
  size: 'medium'
}

Card.propTypes = {
  size: PropTypes.oneOf(['large', 'medium']),
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  backers: PropTypes.number.isRequired, 
  date: PropTypes.instanceOf(Date).isRequired
}

export default Card;

