import React from 'react';
import styled from 'styled-components';
import {lighten} from 'polished';
import theme from '../constants/theme';

const Button = styled.button`
  padding: 0.5rem 0.6rem;
  border-radius: 2px;
  font-size: 1rem;
  margin: 0 1em;
  display: inline-block;
  background-color: ${props=>props.background};
  color: ${props=>props.color};
  border: 1px solid ${props=>props.color}; 
  box-shadow: none;
  cursor: pointer;
  min-width: 5rem;
  &:hover {
    background-color: ${props=>lighten(0.05, props.background)}
  }
`;

Button.defaultProps = {
  color: 'white',
  background: theme.primary
}


export default Button;

