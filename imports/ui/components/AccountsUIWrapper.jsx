import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Template } from 'meteor/templating';
import Blaze from 'meteor/gadicc:blaze-react-component';

class AccountsUIWrapper extends Component {
  render() {
    return (
      <Blaze template="loginButtons" align="right" />
    );
  }
}

export default AccountsUIWrapper;