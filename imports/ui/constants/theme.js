export default {
  success: '#05A8AA',
  danger: '#FF5A5F',
  dark: '#050505',
  primary: '#00B0FF',
  light: '#F4EAF6'
}