import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTracker } from 'meteor/react-meteor-data';
import { Campaigns } from '../api/campaigns.js';
import { Meteor } from 'meteor/meteor';
import Heading from './components/Heading';
import Card from './components/Card';
import Header from './components/Header';

const Section = styled.section`
  width: 90%;
  margin: 0 auto;

  .card-container {
    width: 100%;
    margin: 4em auto;
    position: relative;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-around;

    .single-card {
      flex-basis: 30%;
      margin: 1em auto;
    }
  }
`;

class Discover extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  componentDidMount() {
    Meteor.call('campaigns.find', (error,result)=>{
      if(error) throw error;
      this.setState({
        data: result
      });
    });
  }
  render() {
    return (
      <Section>
        <Header title="DISCOVER PROJECTS" subtitle="Start Exploring"/>
        <section className='card-container'>
          {
            this.state.data.map((campaign)=>
              <div key={campaign._id} className="single-card">
                <Card
                  image={campaign.image}
                  description={campaign.description}
                  title={campaign.title}
                  subtitle={campaign.subtitle}
                  backers={campaign.backers.value}
                  amount={campaign.collected}
                  currency={campaign.currency}
                  total={campaign.amount}
                  date={new Date(campaign.days)}
                />
              </div>
            )
          }
        </section>
      </Section>
    );
  }
}

Discover.defaultProps = {
  campaigns: []
}

export default Discover;