import React from 'react';
import styled from 'styled-components';
import Heading from '../components/Heading';
import Subheading from '../components/Subheading';
import Button from '../components/Button';
import background from './pic.jpg';
import theme from '../constants/theme';

const HeroSection = styled.section`
  width: 100%;
  background-image: url('${background}');
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  min-height: 90vh;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const ActionButton = Button.extend`
  background-color: transparent;
  letter-spacing: 2px;
  font-size: larger;
  border-width: 4px;
  &:hover {
    background-color: transparent;
    border-image: linear-gradient(to right, #0099f7, #f11712) 20% 20%;
  }
`;


function Hero() {
  return (
    <HeroSection>
      <div className="action">
        <Heading color='#ffffff'>Build a Better India</Heading>
        <ActionButton onClick={()=>{document.getElementById('trending').scrollIntoView({behavior: 'smooth'})}}>GET STARTED NOW</ActionButton>
      </div>
    </HeroSection>
  );
}

export default Hero;

