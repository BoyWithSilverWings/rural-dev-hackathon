import React from 'react';
import styled from 'styled-components';
import Card from '../components/Card';
import Subheading from '../components/Subheading';

const Section = styled.section`
  width: 100%;
  margin: 3em 0;

  .content {
    margin: 0 auto;
    width: 90%;
  }

  .card-container {
    display: flex;
    margin-top: 1em;
    justify-content: space-around;
  }

  .smaller-ones {
    width: 25%;
    display: inline-block;
  }
`;

class Trending extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  componentDidMount() {
    Meteor.call('campaigns.trending', (error,result)=>{
      if(error) throw error;
      this.setState({
        data: result
      });
    });
  }
  render() {
    const { data } = this.state;
    return (
      <Section id="trending">
        <div className="content">
        <Subheading margin="0 1em" fontSize="1.5">TRENDING NOW</Subheading>
          {
            data.length!==0
            && <Card
              size="large"
              image={data[0].image}
              description={data[0].description}
              title={data[0].title}
              subtitle={data[0].subtitle}
              backers={data[0].backers.value}
              amount={data[0].collected}
              currency={data[0].currency}
              total={data[0].amount}
              date={new Date(data[0].days)}
            />
          }
          <div className="card-container">
          {
            data.map((campaign, index)=>{
              return ( 
                <div className='smaller-ones' key={campaign._id}>
                  <Card
                  image={campaign.image}
                  description={campaign.description}
                  title={campaign.title}
                  subtitle={campaign.subtitle}
                  backers={campaign.backers.value}
                  amount={campaign.collected}
                  currency={campaign.currency}
                  total={campaign.amount}
                  date={new Date(campaign.days)}
                />
                </div>
              )
            })
          }
          </div>
          </div>
      </Section>
    );
  }
}

export default Trending;