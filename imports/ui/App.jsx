import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {ThemeProvider, injectGlobal} from 'styled-components';
import theme from './constants/theme';
import NavbarContainer from './components/NavbarContainer';
import HomePage from './HomePage';
import Discover from './Discover';
import Page from './Page';
import HowItWorks from './HowItWorks';

injectGlobal`
  body {
    margin: 0;
    font-family: 'Open Sans', sans-serif;
    box-styling: border-box;
    overflow-x: hidden;
  }
`;

class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <ThemeProvider theme={theme}>
          <main>
            <NavbarContainer />
            <Route path='/how-it-works' component={HowItWorks} />
            <Route exact path='/discover/:id' component={Page} />
            <Route exact path='/discover' component={Discover} />
            <Route exact path='/' component={HomePage} />
          </main>
        </ThemeProvider>
      </Router>
    );
  }
}

export default App;