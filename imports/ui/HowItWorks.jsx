import React from 'react';
import styled from 'styled-components';
import Header from './components/Header';
import Subheading from './components/Subheading';
import Button from './components/Button';
import Footer from './components/Footer';

const Section = styled.section`
  margin: 0 auto;
  width: 80%;
`;

const Content = styled.article`
  width: 80%;
  margin: 40px auto;
  line-height: 2rem;
  text-align: justify;
`;

const BigButton = Button.extend`
  width: 80%;
  margin: 40px auto;
  text-transform: uppercase;
  display: block;
`;

function HowItWorks(props) {
  return (
    <>
    <Section>
      <Header title="HOW IT WORKS" subtitle="A peek behind the hood" />
      <Content>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque suscipit dolore doloribus rem, quasi voluptates. Tempora, aliquid? Inventore ab eligendi itaque ratione ex consectetur labore odio eos, a aliquam quasi?
        Quas alias corporis, mollitia explicabo dicta animi? Repellat rerum provident obcaecati tempore adipisci possimus ipsa ab, expedita reiciendis facilis deleniti delectus odit esse nam. Mollitia cupiditate qui vitae iure rem!
        Molestias ut, suscipit exercitationem totam repudiandae magni aspernatur. Laudantium autem nobis voluptatum ex inventore, quod, facilis earum perferendis magni alias deserunt voluptatibus atque! Nam excepturi doloremque corrupti ipsam, quos dolore.
        Saepe dolorem non fugit cumque error est impedit, similique odio fuga doloremque, quia labore? Voluptatem unde dolorum mollitia sequi quam, esse, at ratione dicta distinctio ex dolor ea error facilis?
        Odio incidunt, porro eaque dolor repellendus atque quo corporis eligendi sunt aliquam, iste commodi harum facere quaerat aliquid eum sequi vero voluptatum nisi tempore at impedit necessitatibus neque! Alias, expedita!
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi aspernatur adipisci consequatur ea architecto odit, sint laborum a, alias dolores placeat veniam nostrum corrupti quas distinctio at rerum, soluta temporibus?
          Eum dicta accusamus veniam corporis similique. Deleniti nulla consequuntur hic assumenda cupiditate laborum architecto voluptates vel quidem commodi, perferendis sapiente repellendus incidunt odio tempora facilis minima rem eos! Accusantium, consequuntur.
          Accusantium consectetur amet aliquid provident temporibus. Fugit ea quas veritatis iure recusandae inventore possimus eos natus soluta reiciendis, sed dolor voluptatem, in sunt quo nostrum, ad odio eligendi rem aperiam.
          Architecto doloribus recusandae exercitationem enim aliquam voluptas id culpa, atque asperiores odio. Doloremque temporibus ipsum molestiae laudantium quas nihil odio excepturi, perspiciatis inventore sed. Earum culpa libero asperiores fugit expedita.
          Enim, quod tenetur quidem eum dolores tempora nam, quas aliquam delectus ratione ipsa, voluptatibus nobis voluptas soluta quo excepturi eligendi esse obcaecati inventore fugiat sit eos. Excepturi illum culpa laudantium?
          Quod voluptatibus sequi, quisquam doloremque recusandae ducimus, illo architecto praesentium, officia maxime natus ipsum quae placeat facilis soluta. Ipsam quam rem laborum! Minima excepturi quaerat, tenetur iure corporis necessitatibus rem!
          Nam omnis modi quasi, tenetur error earum iste rem totam reprehenderit? Eaque aspernatur libero iste dolorem accusantium eligendi necessitatibus, ipsa et voluptatum aperiam harum minima ut iure facere assumenda quos?
          Obcaecati, facilis aliquam accusantium eos a reprehenderit placeat ab sequi minima ipsam illo culpa temporibus, delectus architecto aperiam iusto. Reprehenderit odio ad nostrum eos minus qui, vero saepe accusantium rem?
          Tempora ipsam repellendus, similique odio dicta, est rem libero odit, sunt dignissimos saepe architecto animi esse quo? Officiis molestias, quas, omnis ex quidem perferendis natus cumque tempore consequatur eaque eum.
          Sapiente nobis, debitis consequatur fuga odio quas, qui nesciunt neque dolore eius dolorum. Commodi nam facere, ab quas aliquid aperiam, maxime impedit corporis voluptate laudantium fugit iusto ex natus alias.
        </p>
      </Content>
        <Subheading style={{ textAlign: 'center' }}>٭ MAKE A DIFFERENCE NOW ٭</Subheading>
      <BigButton onClick={()=>props.history.push('/#trending')}>
        Get Started Now
      </BigButton>
    </Section>
    <Footer />
    </>
  );
}

export default HowItWorks;

