import React from 'react';
import Hero from './HomePage/Hero';
import Trending from './HomePage/Trending';
import Footer from './components/Footer';

function HomePage() {
  return (
    <main>
      <Hero />
      <Trending />
      <Footer />
    </main>
  );
}

export default HomePage;

