import React from 'react';
import styled from 'styled-components';

const Section = styled.section`
  width: 100%;
  margin: 20px auto;
  display: flex;

  .detail {
    width: 70%;
  }

  .main {
    background-color: #FAFAFA;
    width: 30%;
  }
`;

function OverView() {
  return (
    <Section>
      <div className="detail">
      
      </div>
      <div className="main">
        <p>Main</p>
      </div>
    </Section>
  );
}

export default OverView;

